import { createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import reducer from "./reducers/reducer";

const initialValue = {
    skill: null,
};

const enhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
    reducer,
    initialValue,
    enhancer(applyMiddleware(thunk)),
);
export default store;
