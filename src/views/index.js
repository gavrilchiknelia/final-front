import Collapses from './Collapses';
import ListGroups from './ListGroups';
import Navs from './Navs';
import Popovers from './Popovers';
import Tabs from './Tabs';
import Tooltips from './Tooltips';
import Companies from './Companies';
import Skills from './Skills';
import Languages from "./Categories";
import Spheres from "./Sphere";
import Vacancies from "./Vacancies";
import { CoreUIIcons, FontAwesome, SimpleLineIcons } from './Icons';
import { Alerts, Modals } from './Notifications';
import { Page404, Page500 } from './Pages';
import { Login, Register } from './Authorization';
import HRs from './HRs';
import Administrators from "./Administrators";
import Professions from './Professions';

export {
  CoreUIIcons,
  Page404,
  Page500,
  Register,
  Login,
  Modals,
  Alerts,
  SimpleLineIcons,
  FontAwesome,
  Tooltips,
  Tabs,
  Companies,
  Skills,
  Languages,
  Spheres,
  Vacancies,
  Popovers,
  Navs,
  ListGroups,
  Collapses,
  HRs,
  Administrators,
  Professions,
};

