import React, { Component } from 'react';
import axios from 'axios';
import {  Card, CardBody, CardHeader, Col, Row, } from 'reactstrap';
import { connect } from 'react-redux';
import SelectVacancies from "../Select/SelectVacancies";
import { getAllCategories } from '../../actions';
import ReactPaginate from 'react-paginate';
import _ from 'lodash';
import CategoryList from "./CategoryList";
import Search from "../Search/Search";

class Categories extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowForm: false,
      options: [],
      currentPage: 0,
      search: '',
    };
  }
  componentDidMount() {
    const { getAllCategories } = this.props;
    getAllCategories();
    axios.get("http://localhost:4000/vacancies")
      .then((data) =>  {
        axios.get("http://localhost:4000/categories").then( (data1) => {
          let arr = data.data.result.concat(data1.data.result);
          this.setState({ options: arr})
          }
        )
      })
      .catch( err => console.log(err));
  }

  showFormForAdd = () => {
    let val = !this.state.isShowForm;
    this.setState( {isShowForm: val})
  };

  pageChangeHandler = ({selected}) => (
    this.setState({currentPage: selected})
  );
  searchHandler = search => {
    this.setState({search, currentPage: 0})
  };
  getFilteredData(){
    const { search} = this.state;
    const { category } = this.props;

    if (!search) {
      return category
    }
    let result = category.filter(item => item['categoryName'].toLowerCase().includes(search.toLowerCase())
    );
    if(!result.length){
      result = this.props.category
    }
    return result
  }
  render() {
    const pageSize = 5;
    let { isShowForm, options } = this.state;
    const { category } = this.props;
    let pageCount = 0;
    if (category) {
      pageCount =  Math.ceil(category.length / pageSize);
    }
    const filteredData = this.getFilteredData();
    const displayData = _.chunk(filteredData, pageSize)[this.state.currentPage];
    return (
      <div className="animated fadeIn">
        <Row>
          <Col sm="12" >
            { (category && Array.isArray(category)) && (
              <Search onSearch={this.searchHandler}/>
            )}
            <Card>
              <CardHeader className='d-flex justify-content-between'>
                <strong>Список категорій: </strong>
                <button className='btn btn-pill btn-success' onClick={this.showFormForAdd}>Додати категорію</button>
              </CardHeader>
              <CardBody>
                <div id="accordionCategory">
                  {(!category || !Array.isArray(category)) && (
                    <>Список категорій пустий! Додайте категорію.</>
                  )}
                  {isShowForm && (
                    <SelectVacancies options={options}></SelectVacancies>
                  )}
                  { (category && Array.isArray(category)) && (
                    <div className='pb-4'>
                      <CategoryList category={displayData} options={options}/>
                    </div>
                  )
                  }
                  { category &&
                  (category.length > pageSize
                      ? <ReactPaginate
                        previousLabel={'<'}
                        nextLabel={'>'}
                        breakLabel={'...'}
                        breakClassName={'break-me'}
                        pageCount={pageCount}
                        marginPagesDisplayed={2}
                        pageRangeDisplayed={5}
                        onPageChange={this.pageChangeHandler}
                        containerClassName={'pagination'}
                        activeClassName={'active'}
                        pageClassName="page-item"
                        pageLinkClassName="page-link"
                        previousClassName="page-item"
                        nextClassName="page-item"
                        previousLinkClassName="page-link"
                        nextLinkClassName="page-link"
                        forcePage={this.state.currentPage}
                      /> : null
                  )}

                </div>

              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    category: state.category,
    error: state.error,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getAllCategories: () => dispatch(getAllCategories()),
  }
};

const enhancer = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default enhancer(Categories);
