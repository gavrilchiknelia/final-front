import {
  // getOneSkill,
  getAllSkills } from './skillActions';
import { getAllCategories } from './categoryActions';
import {
  getAllVacancies,
  // getOneVacancy
} from "./vacancyActions";
import { getAllSpheres} from "./sphereActions";
import { getAllProfessions} from "./professionActions";

export {
    // getOneSkill,
  getAllSkills,
  getAllCategories,
  // getOneVacancy,
  getAllVacancies,
  getAllSpheres,
  getAllProfessions,
}
