import axios from 'axios';
import { successAxiosCategory, errorAxiosCategory } from './actions'

const getAllCategories = () => {
    return (dispatch) => {
            axios.get("http://localhost:4000/categories").then((data) => {
                // console.log(data);
            dispatch(successAxiosCategory(data.data.result));
        }).catch((error) => {
            dispatch(errorAxiosCategory(error));
        });
    }
};

export {
  getAllCategories,
}
