const successAxiosSkill = (payload) => {
    return {
      type: 'SUCCESS_AXIOS_SKILL',
      skill: payload,
    };
};
const errorAxiosSkill = (payload) => {
  return {
    type: 'ERROR_SKILL',
    skillError: payload,
  };
};
const successAxiosCategory = (payload) => {
  return {
    type: 'SUCCESS_AXIOS_CATEGORY',
    category: payload,
  };
};

const errorAxiosCategory = (payload) => {
    return {
      type: 'ERROR_CATEGORY',
      categoryError: payload,
    };
};
const successAxiosVacancy = (payload) => {
  return {
    type: 'SUCCESS_AXIOS_VACANCY',
    vacancy: payload,
  };
};

const errorAxiosVacancy = (payload) => {
  return {
    type: 'ERROR_VACANCY',
    vacancyError: payload,
  };
};
const successAxiosSphere = (payload) => {
  return {
    type: 'SUCCESS_AXIOS_SPHERE',
    sphere: payload,
  };
};

const errorAxiosSphere = (payload) => {
  return {
    type: 'ERROR_SPHERE',
    sphereError: payload,
  };
};
const successAxiosProfession = (payload) => {
  return {
    type: 'SUCCESS_AXIOS_PROFESSION',
    profession: payload,
  };
};

const errorAxiosProfession = (payload) => {
  return {
    type: 'ERROR_PROFESSION',
    professionError: payload,
  };
};
export {
  successAxiosSkill,
  errorAxiosSkill,
  successAxiosCategory,
  errorAxiosCategory,
  successAxiosVacancy,
  errorAxiosVacancy,
  successAxiosSphere,
  errorAxiosSphere,
  successAxiosProfession,
  errorAxiosProfession,
}
