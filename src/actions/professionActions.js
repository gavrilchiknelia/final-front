import axios from 'axios';
import { successAxiosProfession, errorAxiosProfession } from './actions'


const getAllProfessions = () => {
  return (dispatch) => {
    axios.get("http://localhost:4000/professions").then((data) => {
        dispatch(successAxiosProfession(data.data.result));
    }).catch((error) => {
      dispatch(errorAxiosProfession(error));
    });
  }
};

export {
  getAllProfessions,
}
