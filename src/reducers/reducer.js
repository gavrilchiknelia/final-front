
const reduser = (state, action) => {
  // console.log(action)
  switch (action.type) {
    case 'SUCCESS_AXIOS_CATEGORY':
      return {
        category: action.category,
      };
    case 'ERROR_CATEGORY':
      return {
        ...state,
        isError: true,
        categoryError: action.categoryError,
      };
    case 'SUCCESS_AXIOS_SKILL':
      return {
        skill: action.skill,
      };

    case "FAILURE_AXIOS":
      return {
        ...state,
        isError: true,
        error: action.payload,
      };

    case 'ERROR_SKILL':
      return {
        ...state,
        isError: true,
        skillError: action.skillError,
      };

    case 'SUCCESS_AXIOS_VACANCY':
      return {
        vacancy: action.vacancy,
      };
    case 'ERROR_VACANCY':
      return {
        ...state,
        isError: true,
        vacancyError: action.vacancyError,
      };
    case 'SUCCESS_AXIOS_SPHERE':
      return {
        sphere: action.sphere,
      };
    case 'ERROR_SPHERE':
      return {
        ...state,
        isError: true,
        sphereError: action.sphereError,
      };
    case 'SUCCESS_AXIOS_PROFESSION':
      return {
        profession: action.profession,
      };
    case 'ERROR_PROFESSION':
      return {
        ...state,
        isError: true,
        professionError: action.professionError,
      };
    default:
      return state;
  }
};
export default reduser;
